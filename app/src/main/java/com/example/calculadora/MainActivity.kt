package com.example.calculadora

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import org.w3c.dom.Text

class MainActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var enum1: EditText
    lateinit var enum2: EditText
    lateinit var tresultado: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var enum1 = this.findViewById<EditText>(R.id.edt_num1)
        var enum2 = this.findViewById<EditText>(R.id.edt_num2)
        var tresultado = this.findViewById<EditText>(R.id.edt_resultado)

        var btnSuma = findViewById<Button>(R.id.btn_suma)
        var btnResta = findViewById<Button>(R.id.btn_resta)
        var btnDivi = findViewById<Button>(R.id.btn_divi)
        var btnMulti = findViewById<Button>(R.id.btn_multi)

        btnSuma.setOnClickListener() {
            var num1: String = enum1.text.toString()
            var num2: String = enum2.text.toString()
            var resultado = (num1.toFloat() + num2.toFloat())
            tresultado.setText(resultado.toString())

        }

        btnResta.setOnClickListener() {
            var num1: String = enum1.text.toString()
            var num2: String = enum2.text.toString()
            var resultado = (num1.toFloat() - num2.toFloat())
            tresultado.setText(resultado.toString())
        }
        btnDivi.setOnClickListener() {
            var num1: String = enum1.text.toString()
            var num2: String = enum2.text.toString()
            var resultado = (num1.toFloat() / num2.toFloat())
            tresultado.setText(resultado.toString())

        }

        btnMulti.setOnClickListener() {
            var num1: String = enum1.text.toString()
            var num2: String = enum2.text.toString()
            var resultado = (num1.toFloat() * num2.toFloat())
            tresultado.setText(resultado.toString())
        }
    }

    override fun onClick(v: View?) {
    }


}


